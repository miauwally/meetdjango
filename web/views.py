from django.shortcuts import render

import misc.httpHelper as httpHelper

import sql.sqlAccounts as sqlAccounts

# Create your views here.
def faceSearch(request):
    acctID = httpHelper.getValue(request, 'acctID')
    account = sqlAccounts.selectByID(acctID)

    page = render(request, 'faceSearch.html', {
        'account': account,
    })

    return page

def home(request):
    page = render(request, 'home.html', {
    })

    return page

def login(request):
    page = render(request, 'login.html', {
    })

    return page

def profile(request):
    acctID = httpHelper.getValue(request, 'acctID')
    account = sqlAccounts.selectByID(acctID)

    page = render(request, 'profile.html', {
        'account': account,
    })

    return page

def register(request):
    page = render(request, 'register.html', {
    })

    return page

# Create your views here.
def accountList(request):
    accounts = sqlAccounts.selectAll()
    page = render(request, 'accountList.html', {
        'accounts': accounts,
    })

    return page
