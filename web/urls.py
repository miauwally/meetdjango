from django.urls import include, path
from django.conf.urls import url, include

import web.views as views

urlpatterns = [
    path('accountList', views.accountList),
    path('faceSearch', views.faceSearch),
    path('home', views.home),
    path('login', views.login),
    path('profile', views.profile),
    path('register', views.register),
    url('.*', views.register),
]
