from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from django.core.files.storage import FileSystemStorage

import json
import os
from passlib.hash import argon2

import misc.httpHelper as httpHelper
import misc.msgHelper as msgHelper
import model.mdAccounts as mdAccounts
import model.mdFace8 as mdFace8
import sql.sqlAccounts as sqlAccounts

class CONST:
    facesetToken = '6c04faa10d484fb59366c8111d022512'

# 人臉搜尋
@csrf_exempt
def faceSearch(request):
    # 獲得實體路徑
    filename = httpHelper.getFile(request, 'image_file')

    # 更新 Face8 的 faceset 內容
    o = mdFace8.search(CONST.facesetToken, filename)
    results = o['results']

    if len(results) <= 0:
        return msgHelper.genErr('no_result')

    result = results[0]
    face_token = result['face_token']

    account = sqlAccounts.selectByFaceToken(face_token)
    o['account'] = account

    return httpHelper.rspJson(o)

@csrf_exempt
def register(request):
    email = httpHelper.getValue(request, 'email')
    name = httpHelper.getValue(request, 'name')
    password = httpHelper.getValue(request, 'password')

    account = sqlAccounts.selectByEmail(email)

    if account:
        return httpHelper.rspError('EMAIL_EXISTED')

    hashed = argon2.using(rounds=4).hash(password)
    sqlAccounts.add(email, name, hashed)

    rsp = {
        'message': 'OK',
    }

    return httpHelper.rspJson(rsp)

@csrf_exempt
def login(request):
    email = httpHelper.getValue(request, 'email')
    password = httpHelper.getValue(request, 'password')

    account = sqlAccounts.selectByEmail(email)

    if not account:
        return httpHelper.rspError('INVALID_EMAIL')

    # if not argon2.verify(password, account['pwd']):
    #     return httpHelper.rspError('INVALID_PASSWORD')

    rsp = {
        'message': 'OK',
        'acctID': account['id'],
    }

    return httpHelper.rspJson(rsp)

@csrf_exempt
def profileUpdate(request):
    # 從 cookies 裡拿到 acctID
    acctID = httpHelper.getValue(request, 'acctID')
    name = httpHelper.getValue(request, 'name')
    password = httpHelper.getValue(request, 'password')

    account = sqlAccounts.selectByID(acctID)

    # 檢查輸入的 acctID 是否存在
    if not account:
        return httpHelper.rspError('INVALID_ACCESS')

    # update 使用者名稱
    sqlAccounts.updateName(acctID, name)

    # 如果有輸入 password，則 update 使用者密碼
    if password:
        hashed = argon2.using(rounds=4).hash(password)
        sqlAccounts.updatePwd(acctID, hashed)

    rsp = {
        'message': 'OK',
    }

    return httpHelper.rspJson(rsp)

# 更換會員的照片
@csrf_exempt
def faceUpdate(request):
    acctID = httpHelper.getValue(request, 'acctID')
    account = sqlAccounts.selectByID(acctID)

    # 檢查 acctID 是否為合法的使用者 
    if not account:
        return httpHelper.rspError('INVALID_ACCESS')

    # 獲得實體路徑
    filename = httpHelper.getFile(request, 'image_file')

    # 以會員 ID 重新命名檔名
    imgPath = os.path.join(settings.TMP_DIR, '{}.jpg'.format(acctID))

    if os.path.isfile(imgPath):
        os.remove(imgPath)

    os.rename(filename, imgPath)

    # 組出檔案在網站內的存取路徑
    urlImg = '/static/{}.jpg'.format(acctID)

    # 將檔案路徑寫入 SQL 資料庫
    sqlAccounts.updateUrlImg(acctID, urlImg)

    # 更新 Face8 的 faceset 內容
    mdAccounts.updateFaceToken(account, imgPath)

    return httpHelper.rspJson()
