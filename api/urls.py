from django.urls import include, path

import api.service as service

urlpatterns = [
    path('login', service.login),
    path('profile/update', service.profileUpdate),
    path('profile/face/update', service.faceUpdate),
    path('register', service.register),
    path('faceSearch', service.faceSearch),
]
