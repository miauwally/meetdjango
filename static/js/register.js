function Register() {
    $('#spHint').text('註冊中...')

    var email = $('#email').val()

    if (!email) {
        $('#spHint').text('請填入 email')
        return
    }
    else if (!isValidEmail(email)) {
        $('#spHint').text('Email 格式不符')
        return
    }
    
    var name = $('#name').val()

    if (!name) {
        $('#spHint').text('請填入姓名')
        return
    }

    var password = $('#password').val()
    var password2 = $('#password2').val()

    if (!password) {
        $('#spHint').text('請填入密碼')
        return
    }
    else if (password != password2) {
        $('#spHint').text('密碼不一致')
        return
    }

    var postData = {
        email: email,
        name: name,
        password: password,
    }

    AjaxPost('/api/register', postData, cbRegister, cbRegisterErr)
}

function cbRegister(ret) {
    $('#spHint').text(ret.message)
}

function cbRegisterErr(ret) {
    err = ret.responseJSON
    $('#spHint').text(err.message)
}
