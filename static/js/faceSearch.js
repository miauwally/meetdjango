$(document).ready(function () {
    $('#uploadImage').on('change', loadImgOnDiv)
  });
  
  function loadImgOnDiv(e) {
    $('#faceRectDiv').empty()
  
    var faceRectDiv = $('#faceRectDiv')
    inputID = e.currentTarget.id
    
    var width = faceRectDiv[0].clientWidth
    
    e.preventDefault()
    e = e.originalEvent
    var target = e.dataTransfer || e.target
    var file = target && target.files && target.files[0]
    
    var options = {
      maxWidth: width,
      canvas: true,
      pixelRatio: window.devicePixelRatio,
      downsamplingRatio: 0.5,
      orientation: true
    }
  
    // check input file
    if (!file) {
      return
    }
  
    //displayImage(file, options)
    currentFile = file
  
    if (!loadImage(file, updateResults, options)) {
      $('#divFileErr').text('上傳圖片失敗，請重新上傳')
    }
  }
  
  function updateResults (img, data) {
    var fileName = currentFile.name
    var href = img.src
    var dataURLStart
  
    if (!(img.src || img instanceof HTMLCanvasElement)) {
      $('#divFileErr').text('上傳圖片失敗，請重新上傳')
    } 
    else {
      
      if (!href) {
        href = img.toDataURL(currentFile.type + 'REMOVEME')
        // Check if file type is supported for the dataURL export:
        dataURLStart = 'data:' + currentFile.type
        /*
        if (href.slice(0, dataURLStart.length) !== dataURLStart) {
          fileName = fileName
        }
        */
      }
    }
  
    $("#faceRectDiv").css("background-image", "url(" + href + ")");
  }
  
  function FaceSearch() {
    var formData = new FormData();
  
    formData.append('image_file', $('#uploadImage')[0].files[0]);
  
    AjaxPostEx("/api/faceSearch", formData, cbFaceSearch, cbFaceSearchErr);
  }
  
  function cbFaceSearch(ret) {
    $('#hintSubmit').text('')
    console.log(ret)
  
    var results = ret.results
  
    if (results.length < 1) {
      $('#hintSubmit').text('沒偵測到人臉')
      return
    }
    else if (results.length > 1) {
      $('#hintSubmit').text('超過一張人臉')
      return
    }
  
    thresholds = ret.thresholds
    threshold = thresholds['1e-5']
  
    var result = results[0]
  
    confidence = result.confidence
    faceToken = result.face_token
    $('#faceToken').text(faceToken)
    $('#confidence').text(confidence)
    $('#threshold').text(threshold)
  
    if (confidence < threshold) {
      $('#hintSubmit').text('沒有搜尋到相符的人臉')
      return
    }

    account = ret.account

    if (account) {
      $('#name').text(account.name)
    }
  }
  
  
  function cbFaceSearchErr(ret) {
    err = ret.responseJSON
    console.log(err)
  }
  