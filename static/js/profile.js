$(document).ready(function () {
  $('#uploadImage').on('change', loadImgOnDiv)
});

function loadImgOnDiv(e) {
  $('#faceRectDiv').empty()

  var faceRectDiv = $('#faceRectDiv')
  inputID = e.currentTarget.id
  
  var width = faceRectDiv[0].clientWidth
  
  e.preventDefault()
  e = e.originalEvent
  var target = e.dataTransfer || e.target
  var file = target && target.files && target.files[0]
  
  var options = {
    maxWidth: width,
    canvas: true,
    pixelRatio: window.devicePixelRatio,
    downsamplingRatio: 0.5,
    orientation: true
  }

  // check input file
  if (!file) {
    return
  }

  //displayImage(file, options)
  currentFile = file

  if (!loadImage(file, updateResults, options)) {
    $('#divFileErr').text('上傳圖片失敗，請重新上傳')
  }
}

function updateResults (img, data) {
  var fileName = currentFile.name
  var href = img.src
  var dataURLStart

  if (!(img.src || img instanceof HTMLCanvasElement)) {
    $('#divFileErr').text('上傳圖片失敗，請重新上傳')
  } 
  else {
    
    if (!href) {
      href = img.toDataURL(currentFile.type + 'REMOVEME')
      // Check if file type is supported for the dataURL export:
      dataURLStart = 'data:' + currentFile.type
      /*
      if (href.slice(0, dataURLStart.length) !== dataURLStart) {
        fileName = fileName
      }
      */
    }
  }

  $("#faceRectDiv").css("background-image", "url(" + href + ")");
}


function ProfileUpdate() {
    // 取得 name 內容
    var name = $('#name').val()

    // 檢查 name 是否有值
    if (!name) {
        $('#spHint').text('請填入姓名')
        return
    }

    var password = $('#password').val()
    var password2 = $('#password2').val()

    // 檢查密碼是否一致
    if (password != password2) {
        $('#spHint').text('密碼不一致')
        return
    }

    var postData = {
        name: name,
        password: password,
    }

    AjaxPost('/api/profile/update', postData, cbProfileUpdate, cbProfileUpdateErr)
}

function cbProfileUpdate(ret) {
    $('#spHint').text(ret.message)
}

function cbProfileUpdateErr(ret) {
    err = ret.responseJSON
    $('#spHint').text(err.message)
}

function FaceUpdate() {
  var formData = new FormData();

  formData.append('image_file', $('#uploadImage')[0].files[0]);

  AjaxPostEx("/api/profile/face/update", formData, cbFaceUpdate, cbFaceUpdateErr);
}

function cbFaceUpdate(ret) {
  location.reload()
}

function cbFaceUpdateErr(ret) {
  err = ret.responseJSON
  console.log(err)
}
