function AjaxPost(url, postData, callback, callbackErr) {
    $.ajax({
      type: "POST",
      url: url,
      data: postData,
      success: function (data) {
        if (callback && typeof (callback) == "function")
          callback(data);
      },
      error: function (err) {
        if (callbackErr && typeof (callbackErr) == "function")
          callbackErr(err);
      }
    });
}

function AjaxPostEx(url, postData, callback, callbackErr) {
  $.ajax({
    url: url,
    type: 'POST',
    cache: false,
    data: postData,
    processData: false,
    contentType: false
  }).done(function(data) {
    if (callback && typeof (callback) == "function")
    callback(data);
  }).fail(function(err) {
    console.log('AjaxPost error', err)

    if (callbackErr && typeof (callbackErr) == "function")
      callbackErr(err);
    else
      cbErr(err)
  });
}


function isValidEmail(email) {
  //Regular expression Testing
  emailRule = /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z]+$/;
  
  //validate ok or not
  if(email.search(emailRule)!= -1){
      return true
  }
  
  return false
}
