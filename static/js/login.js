function Login() {
    $('#spHint').text('登入中...')
    // 檢查 email
    var email = $('#email').val()
    var password = $('#password').val() 

    var postData = {
        email: email,
        password: password,
    }

    AjaxPost('/api/login', postData, cbLogin, cbLoginErr)
}

function cbLogin(ret) {
    $('#spHint').text(ret.message)
    Cookies.set("acctID", ret.acctID);
    location.href = '/web/home'
}

function cbLoginErr(ret) {
    err = ret.responseJSON
    $('#spHint').text(err.message)
}
