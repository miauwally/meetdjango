from django.shortcuts import render
from django.conf import settings

import os
import traceback
import datetime
import json
import random
import string

import misc.httpHelper as httpHelper
import misc.jsonHelper as jsonHelper

def exceptionHandle(request=None):
    url = ""
    dataPost = None
    dataCOOKIES = None

    if request:
        url = request.build_absolute_uri()
        dataPost = jsonHelper.dumpKeyValue(request.POST)
        dataPost = json.dumps(dataPost)

        dataCOOKIES = jsonHelper.dumpKeyValue(request.COOKIES)
        dataCOOKIES = json.dumps(dataCOOKIES)
        print('url:', url)

    tb = traceback.format_exc()
    print('tb:', tb)

    if not os.path.isdir(settings.DIR_EXC):
        os.makedirs(settings.DIR_EXC)

    filename = datetime.datetime.now().strftime('%Y-%m-%d_%H%M.txt')
    filePath = os.path.join(settings.DIR_EXC, filename)

    with open(filePath, 'a') as the_file:
        the_file.write(url + '\n')
        the_file.write(dataPost + '\n')
        the_file.write(dataCOOKIES + '\n')
        the_file.write(tb + '\n')

    error = {
    'error_message' : 'INTERNAL_EXCEPTION',
    }

    return {'error' : error}

def printException(msg):
    print(msg)
    tb = traceback.format_exc()
    print('tb:', tb)

def genAction(message=None, action=None):
    if not message:
        message = ''

    if not action:
        action = ''

    err = {
    'message': message,
    'action' : action,
    }

    return httpHelper.rspJson(err)

def genError(message=None):
    if not message:
        message = 'INTERNAL_EXCEPTION'

    err = {
    'message' : message,
    }

    return httpHelper.rspJson(err, 400)

def genErrPage(request):
    errPage = render(request, settings.ERROR_PAGE, {
    })

    return errPage

def genRandomString(N):
    return ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(N))