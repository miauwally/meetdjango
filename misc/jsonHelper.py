from django.core.serializers.json import DjangoJSONEncoder
import json

def dumps(item):
    return json.dumps(item, sort_keys=True, indent=1, cls=DjangoJSONEncoder)

def dumpByKey(oList, key):
    print('dumpByKey [{}]'.format(key))
    
    for o in oList:
        print(o[key])

def dumpKeyValue(obj):
    print('-- dumpKeyValue --')
    rst = {}

    try:
        for key, value in obj.items() :
            print (key, value)
            rst[key] = value
    except Exception as e:
        print('[except] dumpKeyValue', e)

    return rst
