import os
import json

from django.http import HttpResponse
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django.core.serializers.json import DjangoJSONEncoder

# 將 http 裡的檔案存入 temp folder，並回傳其檔案路徑
def getFile(request, key):
    files = request.FILES.getlist(key)

    if len(files) < 1:
        return None

    fs = FileSystemStorage()

    if not os.path.isdir(settings.TMP_DIR):
        os.makedirs(settings.TMP_DIR)

    f = files[-1]
    filePath = os.path.join(settings.TMP_DIR, f.name)
    filename = fs.save(filePath, f)

    return filename

def getValue(request, key):
    if key in request.GET:
        return request.GET[key]

    if key in request.POST:
        return request.POST[key]

    if key in request.COOKIES:
        return request.COOKIES[key]

    return None

def rspJson(item='', code=200):
    msg = json.dumps(item, sort_keys=False, indent=1, cls=DjangoJSONEncoder)

    return HttpResponse(msg, content_type="application/json", status=code)

def rspError(message='INTERNAL_EXCEPTION', code=400):
    err = {
        'message' : message,
    }

    return rspJson(err, code)
