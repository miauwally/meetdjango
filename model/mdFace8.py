import json
import requests

import misc.msgHelper as msgHelper

from misc import errMsg

class URL:
    detect = 'https://face8.pakka.ai/api/v2/faceDetect'
    search = 'https://face8.pakka.ai/api/v2/search'
    faceSetDelete = 'https://face8.pakka.ai/api/v2/faceset/delete'
    faceSetAddFace = 'https://face8.pakka.ai/api/v2/faceset/addFace'
    faceSetCreate = 'https://face8.pakka.ai/api/v2/faceset/create'
    faceSetCustomData = 'https://face8.pakka.ai/api/v2/face/setCustomData'
    faceSetRemoveFace = 'https://face8.pakka.ai/api/v2/faceset/removeFace'

class RES:
    api_key = '7f6b4d0cab0444f2a81c44140cc31c27'
    facesetToken = '6c04faa10d484fb59366c8111d022512'

def parseResponse(r):
    jsonObj = json.loads(r.text)

    return r.status_code, jsonObj

def detect(filename):
    try:
        payload = {
        'api_key': RES.api_key,
        }

        files = {'image_file': open(filename, 'rb')}

        r = requests.post(URL.detect, files=files, data=payload)
        code, o = parseResponse(r)

        return o
    except:
        msgHelper.exceptionHandle()

    return None

def facesetAddFace(facesetToken, faceToken):
    try:
        payload = {
        'api_key': RES.api_key,
        'faceset_token': facesetToken,
        'face_tokens': faceToken,
        }

        r = requests.post(URL.faceSetAddFace, data=payload)
        code, o = parseResponse(r)

        if code == 200:
            return errMsg.ok
    except:
        msgHelper.exceptionHandle()

    return errMsg.fail_access

def facesetCreate(name):
    try:
        payload = {
        'api_key': RES.api_key,
        'faceset_name': name,
        }

        r = requests.post(URL.faceSetCreate, data=payload)
        code, o = parseResponse(r)

        return o['faceset_token']

    except:
        msgHelper.exceptionHandle()

    return None

def faceSetCustomData(faceSetToken, faceToken, customData):
    try:
        payload = {
        'api_key': RES.api_key,
        'faceset_token' : faceSetToken,
        'face_token' : faceToken,
        'custom_data' : json.dumps(customData),
        }

        r = requests.post(URL.faceSetCustomData, data=payload)
        code, o = parseResponse(r)

        return code, o

    except:
        msgHelper.exceptionHandle()

    return -1, None

def faceSetRemoveFace(facesetToken, faceToken):
    try:
        payload = {
        'api_key': RES.api_key,
        'faceset_token': facesetToken,
        'face_tokens': faceToken,
        }

        r = requests.post(URL.faceSetRemoveFace, data=payload)
        code, o = parseResponse(r)

        if code == 200:
            return errMsg.ok
    except:
        msgHelper.exceptionHandle()

    return errMsg.fail_access

def search(facesetToken, filename):
    try:
        payload = {
        'api_key': RES.api_key,
        'faceset_token': facesetToken,
        }

        files = {'image_file': open(filename, 'rb')}

        r = requests.post(URL.search, files=files, data=payload)
        code, o = parseResponse(r)

        return o
    except:
        msgHelper.exceptionHandle()

    return None
