from misc import errMsg

import model.mdFace8 as mdFace8
import sql.sqlAccounts as sqlAccounts

class CONST:
    facesetToken = '6c04faa10d484fb59366c8111d022512'

# 更新會員的 faceToken
def updateFaceToken(account, filename):
    # 人臉偵測
    o = mdFace8.detect(filename)

    if not o:
        return errMsg.fail_face_detect

    # 取得人臉的資訊
    faces = o['faces']

    # 若圖片中沒偵測到人臉，則回傳錯誤碼
    if len(faces) < 1:
        return errMsg.fail_face_detect

    face = faces[0]

    # 取得 faceToken
    faceToken = face['face_token']

    # 如果會員先前已有 faceToken 存在，則需先從 faceset 中移除
    if account['faceToken']:
        # 從 faceset 中移除 faceToken
        mdFace8.faceSetRemoveFace(CONST.facesetToken, account['faceToken'])

    # 將新的 faceToken 加入 faceset 中
    result = mdFace8.facesetAddFace(CONST.facesetToken, faceToken)

    # 如果添加 faceToken 失敗，則回傳錯誤碼
    if result != errMsg.ok:
        return result

    # 更新會員的 faceToken
    sqlAccounts.updateFaceToken(account['id'], faceToken)

    return errMsg.ok
