import sql.sqlCmd as sqlCmd

def add(email, name, pwd):
    query = ("INSERT INTO meetDjango.accounts "
            "(email, name, pwd) "
            "VALUES (%s, %s, %s)")

    data = (email, name, pwd)
    lastRowID = sqlCmd.insert(query, data)

    return lastRowID

def selectAll():
    query = ("SELECT * FROM meetDjango.accounts;")
    data = None

    rows, fieldNames = sqlCmd.selectEx(query, data)

    if len(rows) < 1:
        return None

    return sqlCmd.dictFormatListCustom(rows, fieldNames)

def selectByEmail(email):
    query = ("SELECT * FROM meetDjango.accounts WHERE email=%s;")
    data = (email, )

    rows, fieldNames = sqlCmd.selectEx(query, data)

    if len(rows) < 1:
        return None

    return sqlCmd.dictFormatCustom(rows[0], fieldNames)

def selectByFaceToken(faceToken):
    query = ("SELECT * FROM meetDjango.accounts WHERE faceToken=%s;")
    data = (faceToken, )

    rows, fieldNames = sqlCmd.selectEx(query, data)

    if len(rows) < 1:
        return None

    return sqlCmd.dictFormatCustom(rows[0], fieldNames)

def selectByID(oID):
    query = ("SELECT * FROM meetDjango.accounts WHERE id=%s;")
    data = (oID, )

    rows, fieldNames = sqlCmd.selectEx(query, data)

    if len(rows) < 1:
        return None

    return sqlCmd.dictFormatCustom(rows[0], fieldNames)

def updateFaceToken(acctID, faceToken):
    query = ("UPDATE meetDjango.accounts SET faceToken=%s WHERE id=%s")
    data = (faceToken, acctID)

    sqlCmd.update(query, data)

def updateName(acctID, name):
    query = ("UPDATE meetDjango.accounts SET name=%s WHERE id=%s")
    data = (name, acctID)

    sqlCmd.update(query, data)

def updatePwd(acctID, password):
    query = ("UPDATE meetDjango.accounts SET pwd=%s WHERE id=%s")
    data = (password, acctID)

    sqlCmd.update(query, data)

def updateUrlImg(acctID, urlImg):
    query = ("UPDATE meetDjango.accounts SET urlImg=%s WHERE id=%s")
    data = (urlImg, acctID)

    sqlCmd.update(query, data)
