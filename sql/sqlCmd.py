import mysql.connector

def connect():
    cnx = mysql.connector.connect(user='root', password='xKi0vE7Pzfuqd2Gn',
    host='34.80.222.20',
    database='meetDjango')
    return cnx

def insert(query, data):
    lastRowID = -1

    try:
        cnx = connect()
        cursor = cnx.cursor()
        cursor.execute(query, data)
        lastRowID = cursor.lastrowid
        cnx.commit()
        cursor.close()
        cnx.close()
    except Exception as e:
        print(e)
        cursor.close()
        cnx.close()

    return lastRowID

def selectEx(query, data):
    rows = []

    try:
        cnx = connect()
        cursor = cnx.cursor()
        cursor.execute(query, data)

        rows = cursor.fetchall()
        fieldNames = [i[0] for i in cursor.description]
        cursor.close()
        cnx.close()
    except Exception as e:
        print('selectEx error, ', e)
        #cursor.close()
        #cnx.close()

    return rows, fieldNames

def dictFormatCustom(obj, fieldNames):
    dictObj = {}

    idx = 0

    for fieldName in fieldNames:
        if obj[idx] is not None:
            dictObj[fieldName] = obj[idx]
        else:
            dictObj[fieldName] = ""

        idx += 1

    return dictObj

def dictFormatListCustom(rows, fieldNames):
    listRst = []

    for obj in rows:
        dictObj = {}

        idx = 0

        for fieldName in fieldNames:
            if obj[idx] is not None:
                dictObj[fieldName] = obj[idx]
            else:
                dictObj[fieldName] = ""

            #dictObj[fieldName] = obj[idx]
            idx += 1

        listRst.append(dictObj)

    return listRst

def update(query, data):
    try:
        cnx = connect()
        cursor = cnx.cursor()
        cursor.execute(query, data)
        cnx.commit()
        cursor.close()
        cnx.close()
    except Exception as e:
        print('except sql update', e)
        cursor.close()
        cnx.close()

        return False

    return True
